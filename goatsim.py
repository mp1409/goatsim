#!/usr/bin/python

import concurrent.futures
import math
import os
import random
import time

NRUNS = 10000000
NRUNS_DESC = 'ten million'


def simulate(nruns, strategy):
	nwins = 0

	for _ in range(nruns):
		doors = set('ABC')
		cardoor = random.choice(tuple(doors))
		selection = random.choice(tuple(doors))
		openeddoor = random.choice(tuple(doors - set(cardoor) - set(selection)))
		doors.remove(openeddoor)

		if strategy == 'change':
			selection = random.choice(tuple(doors - set(selection)))

		if selection == cardoor:
			nwins += 1

	return nwins

with concurrent.futures.ProcessPoolExecutor() as executor:
	print('Simulating {} runs for each strategy.\n'.format(NRUNS_DESC))

	start = time.time()

	target_chunksize = math.ceil(NRUNS / os.cpu_count())
	results = {}

	for strategy in ('stay', 'change'):
		results[strategy] = []
		remaining_runs = NRUNS

		while remaining_runs > 0:
			chunksize = min(target_chunksize, remaining_runs)
			results[strategy].append(executor.submit(simulate, chunksize, strategy))
			remaining_runs -= chunksize

	for strategy in ('stay', 'change'):
		nwins = sum(r.result() for r in results[strategy])
		print('Strategy "{}": {:.2%} cars won.'.format(strategy, nwins / NRUNS))

	print('\nSimulation took us {:.3f} seconds.'.format(time.time() - start))
