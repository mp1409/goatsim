# goatsim
Just-for-fun simulation of the [Monty Hall problem](https://en.wikipedia.org/wiki/Monty_Hall_problem) in Python.

## Usage
```
python3 goatsim.py
```
